FROM ricardocg94/tf-base:latest

# Common variables | versions from binaries 
ARG CLI_VERSION=2.6.8

# metadata of image
LABEL maintainer="Ricardocg <ricguerrero94@gmail.com>"
LABEL oci_cli_version=${CLI_VERSION}

ENV DEBIAN_FRONTEND=noninteractive
ENV CLI_VERSION=${CLI_VERSION}
RUN apt-get update \
    && wget -qO- -O oci-cli.zip "https://github.com/oracle/oci-cli/releases/download/v${CLI_VERSION}/oci-cli-${CLI_VERSION}.zip" \
    && unzip oci-cli.zip \
    && rm *.zip \
    && cd oci-cli \
    && pip3 install oci_cli-*-py2.py3-none-any.whl 

RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD    ["/bin/bash"]